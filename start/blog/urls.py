"""
URL blog Configuration
"""

from django.conf.urls import include, url
from django.contrib import admin
from blog import views
from blog.date import current_datetime
from blog.other_date import hours_ahead


urlpatterns = [
    url(r'^$', views.HomeView.as_view()),
    url(r'^time/$', current_datetime),
    url(r'^time_plus/\d{1,2}/$', hours_ahead)
]
