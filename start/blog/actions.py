from blog.models import Book, Author

def get_authors():
	return [author.first_name for author in Author.objects.all()]

def get_books():
	return [book.name for book in Book.objects.all()]